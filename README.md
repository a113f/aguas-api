# Api para venda de águas.

Esse projeto foi desenvolvido para estudo de AdonisJs que é um framework NodeJS

## Instalação

### Necessário
* Instalar nodeJs 8+ e adonisJs
* Banco usado no projeto foi postgresSql para desenvolvimento e MySql para produção
*Também é necessário, após o download do projeto dar um

 * `npm install` - instala dependências
 * `adonis migration:run` - sobe as migrations do projeto para o banco de dados.



## Uso

* URL_PROD
http://adonisa113f-cc.umbler.net/

Foram criado as seguintes rotas:
```
// Wathers routes
get('/wathers') //All wathers
post('/wathers') //create a new wather
patch('/wathers/:id') // update a wather
delete('/wathers/:id') //delete a wahter
get('/wathers/:id') //delete a wahter

// Users routes
post('/register') // Cadastro de usuários
post('/authenticate') // Login de usuários

// Requests routes
get('/request/:id') //get a request for user_id
post('/request') //create a new req
delete('/request/:id') //Delete a request
```

## Observaçoes:
Quando logar, gerará um token... Esse token deve ser passado em todas as requisições...

## Campos
```
post('/wathers') - {
	"name":"Água x.", - (string)
	"description":"Fazendo o bem para saúde de todos.", - (text)
	"pricing":1.0, - (float)
	"stock":10 - (int)
}

post('/register') - {
	"admin": 0, - (boolean)
	"username":"Allef", - (string)
	"email":"teste@gmail.com", - (string)
	"password":"teste", - (string)
	"adress":"Rua Teste", - (string)
	"cep": "00066345", - (number)
	"house": "121 Altos", - (string)
	"phone":"98587654321" - (string)
}

post('/authenticate') - {
	"email":"teste@gmail.com", - (string)
	"password":"teste" - (string)
}
patch('/wathers/:id') - {
	"description":"Água y" (string)
}
```
## Desenvolvido por
[Allef Gomes](http://allefgomes.000webhostapp.com/)
