'use strict'
const Request = use('App/Models/Request')
const Database = use('Database')
/**
 * Resourceful controller for interacting with requests
 */
class RequestController {
  /**
   * Show a list of all requests if id = user_id
   * GET requests
   */
  async index({ response, params: { id } }) {

    const requests = await Database.select('*').from('requests').where('user_id', id)

    if (requests) {
      response.status(200).json({
        requests
      })
    } else {
      response.status(404).json({
        message: "Não temos requisições até o momento."
      })
    }

  }

  /**
   * Create/save a new request.
   * POST requests
   */
  async store({ request, response }) {
    // get all filds for create a new request
    const data = request.post()
    data.prince = data.amount * data.prince
    // Create a new request
    const req = await Request.create(data)

    response.status(200).json({
      message: "Compra realizada com sucesso."
    })
  }

  /**
   * Display a single request.
   * GET requests/:id
   */
  async show({ params: { id }, request, response}) {
    const req = await Request.find(id)
    
    if(req) {
      response.status(200).json({
        req
      })
    }else{
      response.status(404).json({
        message: "Não existe essa requisição."
      })
    }
  }

  /**
   * Render a form to update an existing request.
   * GET requests/:id/edit
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update request details.
   * PUT or PATCH requests/:id
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a request with id.
   * DELETE requests/:id
   */
  async destroy({ params: { id }, request, response }) {
    const req = await Request.find(id)

    if (req) {
      await req.delete()
      
      response.status(200).json({
        message: "Compra cancelada com sucesso.",
        id
      })
    }else{
      response.status(404).json({
        message: "Compra não encontrada."
      })
    }
  }
}

module.exports = RequestController
