'use strict'
const User = use('App/Models/User')

class UserController {

    async register({ request }) {
        const data = request.post()
        const user = await User.create(data)
        return user
    }

    async authenticate({ request, auth }) {
        const { email, password } = request.all()
        const token = await auth.attempt(email, password)
        return { token, email }
    }

    async index() {
        return { "message": "Bem vindo ao meu mundo!" }
    }

}

module.exports = UserController
