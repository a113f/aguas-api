'use strict'

const Wather = use('App/Models/Wather')

class WatherController {
  async index({ response }) {
    const wathers = await Wather.all()
    if (wathers) {
      response.status(200).json({
        message: "Wathers",
        data: wathers
      })
    } else {
      response.status(404).send({
        message: "Not found."
      })
    }
  }

  async store({ request, response }) {
    const data = request.post()
    const wather = await Wather.create(data)

    response.status(200).json({
      message: "Água adicionada com sucesso.",
      data: wather
    })
  }

  async show({ response, params: { id } }) {
    const wather = await Wather.find(id)

    if(wather) {
      response.status(200).json({
        wather
      })
    }else{
      response.status(404).json({
        message: "Água não encontrada."
      })
    }
  }

  async update({ request, response, params: { id } }) {
    const wather = await Wather.find(id)
    if (wather) {
      const { name, description } = request.post()

      wather.name = name
      wather.description = description

      await wather.save()

      response.status(200)
        .json({
          message: "Água atualizada.",
          wather
        })
    }else {
      response.status(404).json({
        message: "Água não encontrada.",
        id
      })
    }
  }

  /**
   * Delete a wather with id.
   * DELETE wathers/:id
   */
  async destroy({ params: { id }, request, response }) {
    const wather = await Wather.find(id)

    if (wather) {
      await wather.delete()

      response.status(200).json({
        message: "Água deletada com sucesso.",
        id
      })
    }else{
      response.status(404).json({
        message: "Água não encontrada."
      })
    }
  }
}

module.exports = WatherController
