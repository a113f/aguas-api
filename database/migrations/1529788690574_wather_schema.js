'use strict'

const Schema = use('Schema')

class WatherSchema extends Schema {
  up () {
    this.create('wathers', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 50).notNullable()
      table.text('description', 100)
      table.float('pricing').notNullable()
      table.integer('stock')
    })
  }

  down () {
    this.drop('wathers')
  }
}

module.exports = WatherSchema
