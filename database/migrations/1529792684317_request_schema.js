'use strict'

const Schema = use('Schema')

class RequestSchema extends Schema {
  up () {
    this.create('requests', (table) => {
      table.increments()
      table.timestamps()
      table.integer('wather_id').unsigned().references('id').inTable('wathers')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('amount').notNullable()
      table.float('prince').notNullable()
    })
  }

  down () {
    this.drop('requests')
  }
}

module.exports = RequestSchema
