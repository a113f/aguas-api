'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')
Route.get('/', 'UserController.index')

// Wathers routes with token
Route.get('/wathers','WatherController.index').middleware(['auth:jwt']) //All wathers
Route.post('/wathers','WatherController.store').middleware(['auth:jwt']) //create a new wather
Route.patch('/wathers/:id', 'WatherController.update').middleware(['auth:jwt']) // update a wather
Route.delete('/wathers/:id', 'WatherController.destroy').middleware(['auth:jwt']) //delete a wahter
Route.get('/wathers/:id', 'WatherController.show').middleware(['auth:jwt']) //delete a wahter

// Users routes
Route.post('/register','UserController.register')
Route.post('/authenticate','UserController.authenticate')